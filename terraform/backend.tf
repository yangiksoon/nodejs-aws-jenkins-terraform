terraform {
  backend "s3" {
    bucket = "yang-node-aws-jenkins-terraform"
    key = "node-aws-jenkins-terraform.tfstate"
    region = "ap-northeast-2"
  }
}