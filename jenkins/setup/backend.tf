terraform {
  backend "s3" {
    bucket = "yang-node-aws-jenkins-terraform"
    key    = "jenkins.terraform.tfstate"
    region = "ap-northeast-2"
  }
}

